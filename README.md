## Midnight Sun CTF Quals 2020

* **CTF Time page:** https://ctftime.org/event/935
* **Category:** Jeopardy
* **Date:** Fri, 03 April 2020, 16:00 UTC — Sat, 04 April 2020, 16:00 UTC 

---

### Solved Challenges
| Name                       | Category | Points | Key Concepts |
|----------------------------|----------|--------|--------------|
|admpanel                    |pwn       |63      |static analysis, command concatenation|

### Unsolved Challenges to Revisit
| Name                       | Category | Points | Key Concepts |
|----------------------------|----------|--------|--------------|
|pwn1                        |pwn       |        |              |
